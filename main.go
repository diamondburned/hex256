package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/diamondburned/real6cord/vt"
)

func main() {
	log.SetFlags(0)

	if len(os.Args) < 2 {
		log.Fatalln("Missing hex color! Format: #XXXXXX, XXXXXX")
	}

	a := os.Args[1]

	var hex int64
	var err error

	if strings.HasPrefix(a, "#") {
		hex, err = strconv.ParseInt("0x"+a[1:], 0, 64)
	} else {
		hex, err = strconv.ParseInt("0x"+a, 0, 64)
	}

	if err != nil {
		log.Fatalln(err.Error())
	}

	xt, accuracy := vt.GetRGBIntAdv(hex)
	cl := vt.Colors[xt]
	r, g, b := cl.RGB255()

	fmt.Printf(
		"\033[48;5;%dm %d \033[0m\033[38;5;%dm %s \033[0m- Accuracy: %.2f\n",
		xt, xt, xt, fmt.Sprintf("%s rgb(%d, %d, %d)", cl.Hex(), r, g, b), accuracy,
	)
}
